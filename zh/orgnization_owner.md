|No|Organization|Gitee id|Gitee associated email|Role|
|:----: |:----: |:----: |:----: |:----: |
|1|OpenHarmony|https://gitee.com/dingqi_neil|neil.dingqi@huawei.com|社区QA|
|2|OpenHarmony|https://gitee.com/xiaoyang1210|yangna3@huawei.com|社区运营|
|3|OpenHarmony|https://gitee.com/daiyuhong|daiyuhong.dai@huawei.com|社区运营|
|4|OpenHarmony|https://gitee.com/egavrin|evgeny.gavrin@huawei.com|ARKCompiler Maintainer|
|5|OpenHarmony|https://gitee.com/igelhaus|soldatov.anton@huawei.com|ARKCompiler Maintainer|
|6|OpenHarmony|https://gitee.com/Prof1983|ishin.pavel@huawei.com|ARKCompiler Maintainer|
|7|OpenHarmony|https://gitee.com/dmitriitr|trubenkov.dmitrii@huawei.com|ARKCompiler Maintainer|
|8|OpenHarmony|https://gitee.com/ragnvald|dmitry.kovalenko@huawei.com|ARKCompiler Maintainer|
|9|OpenHarmony|https://gitee.com/edachevanton|edachev.anton@huawei-partners.com|ARKCompiler Maintainer|
